# DJVU tools

## E-book workflow

This is one simple bash script and one medium complexity Python module to help me turn ded tree books into e-books.

My workflow is

1. Scan the book
1. Use [ScanTailor](http://scantailor.org/) to get nice tiff images from the scans
1. Do OCR on these with [tesseract](https://tesseract-ocr.github.io/)
1. Correct obvious OCR errors with gImageReader and a text editor.
1. Turn the tif images into a djvu file
1. Make sure the page numbers in the djvu file fit the printed ones
1. Add the OCR text to the djvu
1. Add a table of content to the djvu
1. Add the author name and similar metadata to the djvu

## do-ocr

`do-ocr` is a simple bash script that does point 3 above. Actually it:

* Replaces the file names that come out of ScanTailor. That adds “`_1L`” and “`_2R`” to the output pages when it splits one input into two. `do-ocr` calls my script [fix12LR](https://github.com/ospalh/script-collection/blob/develop/scanning/fix12LR) to fix that. Remove this from the script if yu don’t want that.
* Calls tesseract on all tif files in the working directory
* Works around a gImageReader bug that should be fixed in future releases
* Extract the text into a plain text file
* Combines the hocr ocr output  with hocr-combine
* Removes an xml line that confuses gImageReader

`cd` into the `out` directory of your ScanTailor run and run the script there. If you are unfamiliar with the command line this package is, unfortunately, not for yu

This is, as the saying goes, a pistol. It is easy to shoot yurself in the foot with this. The real problem is that it expects a handful of non-standard tools and will fail if it can’t find them. Apart from my script and tesseract, it needs [hocr-tools](https://github.com/tmbdev/hocr-tools)



## scanToDjvu

Run `scanToDjvu` in the same directory as yu did `do-ocr`, or use the `-d` command line argument to set the directory.

Yu can either run it from the checkt out repo, or yu copy the `scanToDjvu` directory to a place where Python looks for its packages, like `~/.local/lib/python3.7/site-packages/`. It needs PyQt5. Call `runScanToDjvu` to run it. Yu can copy that to `~/bin/` if yu like.

On startup it creates a single page djvu file from each tif images it finds in the working directory. That may take a bit, especially if there are many pages with graphics or images.

When the window pops up, the list of images is on the left. Double-clicking one shows the images in the middle.

The right is divided into four parts. The first two work on images. Click on one on the left to select it and set up that image on the right.

* At the top the three buttons allow yu to use a different single page djvu converter. Standard is “layered”, which tries the bitonal decoder first.
* Next yu can set what djvu calls the title for a page. Click on I use these for page numbers. Give titles like “cover” and “back cover” to those, identify page “i” of the front matter, check "Start series" and “Set title”, and all pages/images after page “i” will get roman numbers as titles. Than find page 1 and do it again for the main part.
* Pick the file you saved your corrected OCR text under here, when you used gImageReader or similar
* Push this button to make the joined djvu file with hidden text and titles to match the printed page numbers
