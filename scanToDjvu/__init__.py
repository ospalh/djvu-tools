# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

"""The init file to run the qapp"""

import argparse

from .collector import runCollector

__version__ = "0.0.1"
__all__ = ["scanToDjvu"]

def scanToDjvu(argv=None):
    if not argv:
        argv = ['scanToDjvu']
    parser = argparse.ArgumentParser(
        description="""Program to turn scanned images into one djvu file""")
    parser.add_argument(
        '-s', '--suffix', type=str, default='.tif',
        help='''Suffix of the files to use''')
    parser.add_argument(
        '-d', '--directory', type=str, default='.',
        help='''Directory where we operate''')
    args = parser.parse_args()
    runCollector(args, argv)
