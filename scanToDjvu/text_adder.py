# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

import os
import re
import sys
import xml.etree.ElementTree as ET

from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QGridLayout, QLabel, QPushButton, QWidget, \
    QFrame, QLineEdit, QFileDialog


# We could be using BS instead of ET:
#from bs4 import BeautifulSoup as BS
#with open(filename) as hf:
#    wsbs = BS(hf.read())

hocr_file_filter = 'hOCR files (*.html *.hocr *.xml)'
all_files_filter = 'all files (*)'
hocr_bbox_re = re.compile(
    r'(?:^|;)\s*bbox\s+(?P<x0>\d+?)\s+(?P<y0>\d+?)\s+' \
    r'(?P<x1>\d+?)\s+(?P<y1>\d+?)\s*(?:;|$)')
hocr_image_re = re.compile(r'(?:^|;)\s*image\s+[\'"](.*?)["\']\s*(?:;|$)')
hocr_res_re = re.compile(r'(?:^|;)\s*res\s+(?P<res>\d+?)\s*(?:;|$)')
djvu_class_dict = {
    'ocr_page': 'page', 'ocr_carea': 'column', 'ocr_par': 'para',
    'ocr_xline': 'line', 'ocrx_line': 'line', 'ocr_word': 'word',
    'ocrx_word': 'word'}

class TextAdder(QFrame):

    def __init__(self, images_list):
        super(QFrame, self).__init__()
        self.layout = QGridLayout(self)
        self.top_label = QLabel(self.tr('Use corrected hOCR file '))
        # + \ '(<span style="font-family: Noto Color Emoji;">❌</span>)')
        self.layout.addWidget(self.top_label, 0, 0, 1, 2)
        self.hocr_line = QLineEdit()
        self.layout.addWidget(self.hocr_line, 1,0, 1,2)
        self.pick_file = QPushButton(self.tr('Choose'))
        self.pick_file.clicked.connect(self.setfilename)
        self.layout.addWidget(self.pick_file, 2, 0)

        self.images_list = images_list
        self.pages_dict = dict()
        self.hight = 0
        self.res = 1.0
        self.page_tree = None
        self.page_txt = ''
        self.depth_spacer = ''  # Keep track how many spaces we should
        # add at the front of each djvu text element we write.

    def iy(self, y):
        # We have to turn all y values around, per page
        return int((self.hight - y) / self.res)

    def ix(self, x):
        return int(x / self.res)

    def ibbox(self, title):
        '''Extract the four int of a hOCR bbox and turn them into a djvu bbox

        Take the title attribute of a hOCR element like ocr_page, scan
        for the four x0, y0, x1, y1 values of the bbox, turn the y’s
        around (0 at the top → y at the bottom) and return just the
        four values as a string.
        '''
        bboxs = hocr_bbox_re.search(title)
        if bboxs is None:
            return None
        # N.B.: the x values never go thru a str → int → str
        # conversion, the y’s do. They also swap positions, so that
        # the smaller value comes first in djvu format. Note the
        # y0=f(y1) and y1=f(y0).
        return '{x0} {y0} {x1} {y1}'.format(
            x0=self.ix(int(bboxs.group('x0'))),
            y0=self.iy(int(bboxs.group('y1'))),
            x1=self.ix(int(bboxs.group('x1'))),
            y1=self.iy(int(bboxs.group('y0'))))

    def bboxhight(self, title):
        bboxs = hocr_bbox_re.search(title)
        if bboxs is None:
            return None
        #print('use hight {}'.format(int(bboxs.group('y1'))), file=sys.stderr)
        return int(bboxs.group('y1'))

    def pageres(self, title):
        pres = hocr_res_re.search(title)
        if pres is None:
            # print('no res found in {}'.format(title), file=sys.stderr)
            return None
        # print('use res {}'.format(int(pres.group('res'))), file=sys.stderr)
        return int(pres.group('res'))

    def txtforitem(self, image_item):
        if not image_item:
            return
        self.getpagetree(image_item)
        if not self.page_tree:
            print('No OCR for {}'.format(image_item.display_name), file=sys.stderr)
            return None
        page_title = self.page_tree.get('title')
        if not page_title:
            # When we’ve gon this far we really should expect a proper
            # ocr_page.
            raise ValueError('No title attribute in ocr_page')
        self.hight = self.bboxhight(page_title)
        self.res = self.pageres(page_title)
        if self.res:
            self.res = self.res / 100
        else:
            self.res = 1
        self.page_txt = ''
        self.addtxtfortag(self.page_tree)
        # print('So paget text is {}'.format(self.page_txt))
        return self.page_txt


    def addtxtfortag(self, a_tag):
        try:
            # These four should all work if this is a

            ocr_class = a_tag.get('class')
            djvu_class = djvu_class_dict[ocr_class]
            tag_title = a_tag.get('title')
            dbbox = self.ibbox(tag_title)
        except (KeyError, ValueError):
            djvu_class = None
            dbbox = None
        if djvu_class:
            self.page_txt += '{spc}({cls} {bbx}'.format(
                spc=self.depth_spacer, cls=djvu_class, bbx=dbbox)
            if a_tag.text is not None:
                tag_text = a_tag.text.strip()
            else:
                # Is this new? tags with text as None instead of ""?
                tag_text = ''
            if tag_text and djvu_class != 'word':
                print('Found text “{}” for non-word tag {}.'.format(
                    tag_text, djvu_class), file=sys.stderr)
            # if tag_text:
            if djvu_class == 'word':
                if not tag_text:
                    print('Adding empty word.', file=sys.stderr)
                # Huh? I checked, we had no »"« in our OCR text. Should be fine.
                # Or not. We had a single backslash. And we want, say,
                # \n in the text to show up as two characters, U+005c, U+006e.
                tag_text = tag_text.replace('\\', '\\\\')
                tag_text = tag_text.replace('"', '\\"')

                self.page_txt += ' "{}"'.format(tag_text)
            if a_tag:
                # This checks if the tag as children.
                self.page_txt += '\n'
                self.depth_spacer += ' '
        for sub_tag in a_tag:
            # True recursion. Should not go too deep. Also done when
            # we didn’t find a djvu_class.
            self.addtxtfortag(sub_tag)
        if djvu_class:
            if a_tag:
                self.depth_spacer = self.depth_spacer[:-1]
                self.page_txt += self.depth_spacer
            self.page_txt += ')\n'

    def getpagetree(self, image_item):
        image_name = os.path.basename(image_item.filename)
        try:
            self.page_tree = self.pages_dict[image_name]
            return
        except KeyError:
            self.page_tree = None
        page_hocr_name = os.path.splitext(image_item.filename)[0] + '.hocr'
        print('No OCR page in single OCR file. '\
              'Trying individual file {}.'.format(page_hocr_name),
              file=sys.stderr)
        try:
            single_page_tree = ET.parse(page_hocr_name)
            single_page_root = single_page_tree.getroot()
        except (FileNotFoundError, ET.ParseError, TypeError):
            self.page_tree = None
            return
        self.page_tree = single_page_root.find(".//*[@class='ocr_page']")

    def getpagesdict(self):
        # self.pages_dict = dict()
        try:
            all_tree = ET.parse(self.hocr_name)
            all_root = all_tree.getroot()
        except (FileNotFoundError, ET.ParseError, TypeError):
            return
        for a_page in all_root.findall(".//*[@class='ocr_page']"):
            try:
                page_image_name = re.search(hocr_image_re,
                    a_page.get('title')).group(1)
            except (AttributeError, TypeError):
                continue
            page_basename = os.path.basename(page_image_name)
            # Cut off an extra “./” from the front. We only deal with
            # files in the current directory and with *just* the
            # basename.
            self.pages_dict[page_basename] = a_page
        # Oh, we don’t keep the tree as one entity after this. Only
        # the pages dictionary. We grab pages out of it as needed.

    @property
    def hocr_name(self):
        return self.hocr_line.text()

    @pyqtSlot()
    def setfilename(self):
        start_name = os.path.join(
            self.images_list.list_dir, (self.images_list.book_title + '.html'))
        new_name, dummy_filter = QFileDialog.getOpenFileName(
            self, self.tr('Pick sigle hOCR file'), directory=start_name,
            filter=hocr_file_filter + '\n' + all_files_filter,
            initialFilter=hocr_file_filter)
        self.hocr_line.setText(new_name)
        self.getpagesdict()
