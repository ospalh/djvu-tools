# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

from PyQt5.QtWidgets import QListView



class ImageListView(QListView):

    def __init__(self, image_list):
        super(QListView, self).__init__()
        self.setModel(image_list)
