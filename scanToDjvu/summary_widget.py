# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QGridLayout, QLabel, QPushButton, QWidget, QFrame

class SummaryWidget(QFrame):

    def __init__(self):
        super(QWidget, self).__init__()
        self.layout = QGridLayout(self)
        self.hav_all_djvu = QLabel(
            '<span style="font-family: Noto Color Emoji;">❌</span> ' +
            self.tr('djvu missing'))
        self.layout.addWidget(self.hav_all_djvu, 0, 0)
        self.all_hav_title = QLabel(
            '<span style="font-family: Noto Color Emoji;">❌</span> ' +
            self.tr('tiles missing'))
        self.layout.addWidget(self.all_hav_title, 0, 1)
        self.status_label = QLabel(
            '<span style="font-family: Noto Color Emoji;">？</span> ')
        self.layout.addWidget(self.status_label, 0, 2)
        self.make_djvu = QPushButton(self.tr('Make book'))
        self.make_djvu.setEnabled(False)
        self.layout.addWidget(self.make_djvu, 1, 0)

    # @pyqtSlot(bool)
    def updateAllDjvu(self, hav_all):
        if hav_all:
            self.hav_all_djvu.setText(
                '<span style="font-family: Noto Color Emoji;">🙆</span> ' +
                self.tr('all djvu'))
            self.make_djvu.setEnabled(True)
        else:
            self.hav_all_djvu.setText(
                '<span style="font-family: Noto Color Emoji;">❌</span> ' +
                self.tr('djvu missing'))
            self.make_djvu.setEnabled(False)


    def updateAllTitle(self, all_hav):
        if all_hav:
            self.all_hav_title.setText(
                '<span style="font-family: Noto Color Emoji;">🙆</span> ' +
                self.tr('all titles'))
        else:
            self.all_hav_title.setText(
                '<span style="font-family: Noto Color Emoji;">❌</span> ' +
                self.tr('titles missing'))

    @pyqtSlot()
    def showOk(self):
        self.status_label.setText(
            '<span style="font-family: Noto Color Emoji;">🙆</span> ')

    # There is no really easy way to check if we have an ocr page for
    # eeach image, so no simbol then.
    # def updateAllOcr(self, all_hav):
    #     pass
