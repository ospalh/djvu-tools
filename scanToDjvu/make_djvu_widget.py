# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

import sys

from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QGridLayout, QLabel, QPushButton, QWidget, QFrame

class MakeDjvuWidget(QFrame):
    maybeMoreDjvus = pyqtSignal()

    def __init__(self):
        super(QFrame, self).__init__()
        self.layout = QGridLayout(self)
        self.top_label = QLabel(
            self.tr('Make djvu for ') + \
            '(<span style="font-family: Noto Color Emoji;">❌</span>)')
        self.layout.addWidget(self.top_label, 0, 0, 1, 2)
        self.make_foto = QPushButton(self.tr('foto'))
        self.make_foto.clicked.connect(self.tryFoto)
        self.layout.addWidget(self.make_foto, 1, 0)
        self.make_diagram = QPushButton(self.tr('diagram'))
        self.make_diagram.clicked.connect(self.tryDiagram)
        self.layout.addWidget(self.make_diagram, 1, 1)
        self.make_layered = QPushButton(self.tr('layered'))
        self.make_layered.clicked.connect(self.tryLayered)
        self.layout.addWidget(self.make_layered, 2, 0)
        self.image = None

    def setImage(self, image):
        self.image = image
        if not image:
            good_djvu = False
        else:
            good_djvu = self.image.hav_djvu
        if good_djvu:
            self.top_label.setText(
                self.tr('Make djvu for ') + \
                '(<span style="font-family: Noto Color Emoji;">🙆</span>)')
        else:
            self.top_label.setText(
                self.tr('Make djvu for ') + \
                '(<span style="font-family: Noto Color Emoji;">❌</span>)')

    @pyqtSlot()
    def tryLayered(self):
        if self.image:
            self.image.makeBitonal(True)
            if not self.image.hav_djvu:
                print("Remake layered djvu for non-bitonal image {}".format(
                    self.image.display_name), file=sys.stderr)
                self.image.makeLayered()
            self.setImage(self.image)  # To update the icon
            self.maybeMoreDjvus.emit()

    @pyqtSlot()
    def tryDiagram(self):
        if self.image:
            self.image.makeDiagram()
            self.setImage(self.image)  # To update the icon
            self.maybeMoreDjvus.emit()

    @pyqtSlot()
    def tryFoto(self):
        if self.image:
            self.image.makeFoto()
            self.setImage(self.image)  # To update the icon
            self.maybeMoreDjvus.emit()
