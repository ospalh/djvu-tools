# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

import os
import re
import tempfile
import subprocess
import sys

image_dpi = 600
djvu_suffix = '.djvu'

class ImageItem(object):

    def __init__(self, filename):
        self.filename = filename
        self.tried_bitonal = False
        self.tried_layered = False
        self.tried_color = False
        self.title = None
        if not self.hav_djvu:
            self.makeBitonal()
        if not self.hav_djvu:
            print("Make layered djvu for non-bitonal image {}".format(
                self.display_name), file=sys.stderr)
            self.makeLayered()


    def __str__(self):
        return self.filename

    def __lt__(self, otherItem):
        return str(self) < str(otherItem)

    @property
    def display_name(self):
        return os.path.basename(self.filename)


    @property
    def djvu_name(self):
        return os.path.splitext(self.filename)[0] + '.djvu'

    @property
    def hav_djvu(self):
        return os.path.isfile(self.djvu_name)

    @property
    def suggested_title(self):
        if self.title:
            return self.title
        display_nums = re.search(r'\b(\d+)\b', self.display_name)
        if display_nums:
            # Strip leading zeros the hard way. Or the easy way. IDK.
            return str(int(display_nums.group(1)))
        return None


    def makeBitonal(self, try_remove=False):
        if try_remove:
            try:
                os.remove(self.djvu_name)
            except FileNotFoundError:
                pass
        subprocess.call(['cjb2', '-lossy', self.filename, self.djvu_name])
        self.tried_bitonal = True


    def makeLayered(self):
        # We need a number of temp files here
        # The way to do it was inspired by djvubind, but simplifies it
        # a bit djvubind:
        # • put the black and white bits in one file
        # • white and the rest in a second file, a tif for some odd reason
        # • encoded the b/w file with cjb2
        # • called a djvu *de*code on that to get a rle file
        # • called convert a second time on the tif output from step two
        # • copied those two into a new file one after the other, 1024
        #   char block by block
        # • called csepdjvu on that
        #
        # So, we do
        # • Separate the b/w bits, as above
        # • white and the rest in a second file, a ppm directly
        # • Still do cjb2 and ddjvu on the b/w bit. When we switch on
        #   “lossy”, we get good enuf output that is, in an example
        #   case, more than a quarter smaller.
        # • Use a subprocess call to cat the two files
        # • Finally, csepdju encode
        # We also remove the temp files as soon as possible.
        self.tried_layered = True
        textpbm = tempfile.NamedTemporaryFile(
            suffix=".pbm", prefix='text', delete=False)
        subprocess.call([
            'convert', self.filename, '+opaque', 'black', '-monochrome',
            textpbm.name])
        # Only the background (white) and everything truly black
        textdjvu = tempfile.NamedTemporaryFile(
            suffix=djvu_suffix, prefix='text', delete=False)
        subprocess.call(['cjb2', '-lossy', textpbm.name, textdjvu.name])
        # The textpbm has been converted and is no longer needed
        os.remove(textpbm.name)
        textrle = tempfile.NamedTemporaryFile(
            suffix=".rle", prefix='text', delete=False)
        subprocess.call(['ddjvu', '-format=rle', textdjvu.name, textrle.name])
        # Convert the lossy djvu to some run length encoding intermediary.
        os.remove(textdjvu.name)
        # And, with the rle file, the temporary djvu can go
        # Now the other bit, the color
        colorppm = tempfile.NamedTemporaryFile(
            suffix=".ppm", prefix='color', delete=False)
        subprocess.call([
            'convert', self.filename, '-opaque', 'black', colorppm.name])
        # That is, the file without everything truly black
        with tempfile.NamedTemporaryFile(prefix='rleppm', delete=False) as rleppm:
            subprocess.call(['cat', textrle.name, colorppm.name], stdout=rleppm)
        # With the two layers in one file, the individual ones can go
        os.remove(textrle.name)
        os.remove(colorppm.name)
        subprocess.call([
            'csepdjvu', '-d',  str(image_dpi), rleppm.name, self.djvu_name])
        # Finally, make the layered djvu
        os.remove(rleppm.name)
        # And remove the last temp file

    def makeDiagram(self):
        # Now we have three make djvus.
        self.tried_color = True
        with tempfile.NamedTemporaryFile(
                suffix=".ppm", prefix='diatemp') as temppm:
            # Eh, we call external programs anyway. Instead of mucking
            # around with pymagick, we do one more external call to
            # “convert”. The Right Thing would be to use the dpi, but
            # as this is just for me i just assume it’s the one i
            # always use, 600. See above.
            subprocess.call(['convert', self.filename ,temppm.name])
            subprocess.call([
                'cpaldjvu', '-dpi',  str(image_dpi), temppm.name,
                self.djvu_name])
        # And poof, the tempfile is gone


    def makeFoto(self):
        self.tried_color = True
        with tempfile.NamedTemporaryFile(
                suffix='.ppm', prefix='fototemp') as temppm:
            subprocess.call(['convert', self.filename ,temppm.name])
            subprocess.call([
                'c44', '-dpi',  str(image_dpi), temppm.name, self.djvu_name])
            # Only we use c44 here, not cpaldjvu.
