# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

import os
import re
import roman
import subprocess
import sys
import tempfile

from glob import glob

from PyQt5.QtCore import QAbstractListModel, QVariant, Qt, QModelIndex, pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QFileDialog, QMessageBox

from .image_item import ImageItem

# We could re-use the one from the item. Meh.
djvu_suffix = '.djvu'

class ImageListModel(QAbstractListModel):
    djvuSaved = pyqtSignal()

    def __init__(self, args, parent):
        super(QAbstractListModel, self).__init__(parent)
        self.list_dir = args.directory
        self.image_items = []
        self.read_items(args.suffix)
        self.image_items.sort()
        self.txt_source = None
        # self.image_items.append('dummy item')

    def read_items(self, suffix):
        for new_path in glob(os.path.join(self.list_dir, '*' + suffix)):
            new_item = ImageItem(new_path)
            if new_item:
                self.image_items.append(new_item)

    def rowCount(self, parent):
        return len(self.image_items)

    def getItem(self, index):
        return self.image_items[index.row()]

    @property
    def all_djvus(self):
        return all(it.hav_djvu for it in self.image_items)

    @property
    def all_title(self):
        return all(it.title for it in self.image_items)

    @property
    def book_title(self):
        try:
            bn = os.path.splitext(
                self.image_items[len(self.image_items)//2].display_name)[0]
            # A (actually the) page in the middle.
        except IndexError:
            return None
        # So fon a page called “Fahrenheit 451 017” we return
        # “Fahrenheit”, but, depending on how careful we hav chosen
        # the file names, we return sensible names for many other
        # books.
        return bn.rstrip('0123456789 _.')

    @pyqtSlot(str, ImageItem)
    def setTitlesSeries(self, title, item):
        try:
            start_idx = self.image_items.index(item)
        except IndexError:
            print('Element not in list. Odd.', file=sys.stderr)
            return
        eurab_groups = re.search(r'^(.*?)(\d+)(.*?)$', title)
        if eurab_groups:
            tnum = int(eurab_groups.group(2))
            for idx in range(start_idx, len(self.image_items)):
                self.image_items[idx].title = eurab_groups.group(1) + \
                    str(tnum) +  eurab_groups.group(3)
                tnum += 1
            return
        if title == title.upper():
            try:
                trnum = roman.fromRoman(title)
            except roman.InvalidRomanNumeralError:
                return
            for idx in range(start_idx, len(self.image_items)):
                self.image_items[idx].title = roman.toRoman(trnum)
                # There is a small chance that this whole thing blows
                # up: when we run out of roman numbers. In that case i
                # think that is the right thing to do.
                trnum += 1
            return
        if title == title.lower():
            try:
                turnum = roman.fromRoman(title.upper())
            except roman.InvalidRomanNumeralError:
                return
            for idx in range(start_idx, len(self.image_items)):
                self.image_items[idx].title = roman.toRoman(turnum).lower()
                turnum += 1
        # There is a significant chance that we just fall out at the
        # end here. If the title does not *contain* a standard number
        # (euro-arabian), or if it *is* not a roman number. E.g. a
        # word that is not “I” (1), “mix” (1009), or a few others.


    def data(self, index, role):
        """Return The “data” in the Qt model/view sense
        That is, the shortened file name. """
        if (role == Qt.DisplayRole):
            item = self.image_items[index.row()]
            return QVariant(item.display_name)
        else:
            return QVariant()

    @pyqtSlot()
    def make_book(self):
        if not self.all_title:
            make_aniway = QMessageBox.information(
                None, self.tr('Not all pages hav a title'),
                self.tr('Continue without page titles?'),
                QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
            if make_aniway == QMessageBox.No:
                return
        filter_string_d = 'djvu files (*' + djvu_suffix + ')'
        filter_string_a = 'all files (*)'
        save_name , dummy_filter = QFileDialog.getSaveFileName(
            caption=self.tr('Book file name'),
            directory=os.path.join(
                self.list_dir, self.book_title + djvu_suffix),
            filter=filter_string_d + '\n' + filter_string_a,
            initialFilter=filter_string_d)
        # The file dialog is doing the annoying about existing files,
        # so we don’t hav to
        join_ok = subprocess.call(
            ['djvm', '-c', save_name] + \
            [ii.djvu_name for ii in self.image_items])
        if join_ok != 0:
            QMessageBox.warning(
                None, self.tr('Djvu problem'),
                self.tr('Joining djvu pages failed.'))
            return
        if self.all_title:
            with tempfile.NamedTemporaryFile(
                    mode='wt+', encoding='utf-8', suffix='.sed',
                    prefix='djvutemp', delete=False) as tempsed:
                for idx, ii in enumerate(self.image_items, start=1):
                    tempsed.write('select {}\n'.format(idx))
                    tempsed.write('set-page-title "{}"\n'.format(ii.title))
                tempsed.write('save\n')
            titles_ok = subprocess.call(
                ['djvused', save_name, '-f', tempsed.name])
            os.remove(tempsed.name)
        if self.txt_source:
            at_least_one_txt = False
            with tempfile.NamedTemporaryFile(
                    mode='wt+', encoding='utf-8', suffix='.sed',
                    prefix='djvutemp', delete=False) as tempsed:
                for idx, ii in enumerate(self.image_items, start=1):
                    page_txt = self.txt_source.txtforitem(ii)
                    if not page_txt:
                        continue
                    at_least_one_txt = True
                    tempsed.write('select {}\n'.format(idx))
                    tempsed.write('remove-txt\n')
                    tempsed.write('set-txt\n')
                    tempsed.write(page_txt)
                    tempsed.write('\n.\n\n')
                tempsed.write('save\n')
            if at_least_one_txt:
                txt_ok = subprocess.call(
                    ['djvused', save_name, '-f', tempsed.name])
            os.remove(tempsed.name)  # Comment this out to test the hidden text stuff
        self.djvuSaved.emit()
