# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

import sys

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel


class ImageLabel(QLabel):
    max_size = 600
    min_width = 200
    def __init__(self):
        super(QLabel, self).__init__()


    def showImage(self, filename):
        new_pixmap = QPixmap(filename)
        if not new_pixmap.isNull():
            # new_pixmap.scaledContents = True
            self.setPixmap(new_pixmap.scaled(
                self.sizeHint(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        else:
            print("Problem showing {}".format(filename), file=sys.stderr)

    def sizeHint(self):
        return QSize(ImageLabel.max_size, ImageLabel.max_size)

    def maxSize(self):
        return QSize(ImageLabel.max_size, ImageLabel.max_size)

    def minSize(self):
        return QSize(ImageLabel.min_width, 10)
