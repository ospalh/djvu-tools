# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

from PyQt5.QtWidgets import QApplication

from .collector_window import CollectorWindow
from .image_list_model import ImageListModel




def runCollector(args, argv):
    collector_app = QApplication(argv)
    # image_list = getImageList(args.directory)
    image_list = ImageListModel(args, parent=None)
    collector_window = CollectorWindow(image_list)
    collector_window.show()
    collector_app.exec()
