# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QGridLayout, QLabel, QPushButton, QWidget, QFrame, QLineEdit, QCheckBox

from .image_item import ImageItem

class TitlesWidget(QFrame):
    seriesTitleChange = pyqtSignal(str, ImageItem)
    maybeMoreTitles = pyqtSignal()

    def __init__(self):
        super(QWidget, self).__init__()
        self.image = None
        self.layout = QGridLayout(self)
        self.title_label = QLabel(
            self.tr('Page title ') + \
            '(<span style="font-family: Noto Color Emoji;">❌</span>)')
        self.layout.addWidget(self.title_label, 0,0, 1,2)
        self.title_edit = QLineEdit()
        self.layout.addWidget(self.title_edit, 1,0, 1,2)
        self.series_box = QCheckBox(self.tr('Start series'))
        self.layout.addWidget(self.series_box, 3,0)
        self.set_button = QPushButton(self.tr('Set title'))
        self.set_button.clicked.connect(self.titleClicked)
        self.layout.addWidget(self.set_button, 3,1)

    def setImage(self, image):
        self.image = image
        if not self.image:
            good_title = False
        else:
            good_title = self.image.title
        if good_title:
            self.title_edit.setText(self.image.title)
            self.title_label.setText(
                self.tr('Page title ') + \
                '(<span style="font-family: Noto Color Emoji;">🙆</span>)')
        else:
            if self.image:
                self.title_edit.setText(self.image.suggested_title)
            else:
                self.title_edit.setText('')
            self.title_label.setText(
                self.tr('Page title ') + \
                '(<span style="font-family: Noto Color Emoji;">❌</span>)')


    @pyqtSlot()
    def titleClicked(self):
        if not self.image:
            return
        if self.series_box.isChecked():
            self.seriesTitleChange.emit(self.title_edit.text(), self.image)
        else:
            self.image.title = self.title_edit.text()
        self.maybeMoreTitles.emit()
        self.setImage(self.image)  # to update the indicator
