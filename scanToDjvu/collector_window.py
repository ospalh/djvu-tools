# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

from PyQt5.QtCore import QSize, QModelIndex, pyqtSlot
from PyQt5.QtGui import  QKeySequence
from PyQt5.QtWidgets import QAction, QMainWindow, QShortcut, \
    QHBoxLayout,  QWidget, QVBoxLayout, QPushButton, QSpacerItem, QSizePolicy, QFrame

from .image_label import ImageLabel
from .image_list_model import ImageListModel
from .image_list_view import ImageListView
from .make_djvu_widget import MakeDjvuWidget
from .summary_widget import SummaryWidget
from .text_adder import TextAdder
from .titles_widget import TitlesWidget

class CollectorWindow(QMainWindow):
    """The three panel information collector class

    This should show a list with the tif images at the left, the
    selected image in the middle and information about it on the
    right."""

    def __init__(self, image_list, *args, **kwargs):
        super(QMainWindow, self).__init__(*args, **kwargs)
        self.image_list = image_list  # This is the model.
        self.setupLayout()
        self.setupConnections()

    def setupLayout(self):
        quit_shortcut = QShortcut(QKeySequence("Ctrl+q"), self, self.close)
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)
        self.central_layout = QHBoxLayout(self.central_widget)
        self.central_widget.setLayout(self.central_layout)
        self.image_list_view = ImageListView(self.image_list)
        self.central_layout.addWidget(self.image_list_view)
        left_line = QFrame()
        left_line.setFrameShape(QFrame.VLine)
        left_line.setSizePolicy(QSizePolicy.Expanding , QSizePolicy.Minimum)
        left_line.setLineWidth(2)
        left_line.setMinimumSize(3,0)
        self.central_layout.addWidget(left_line)
        self.image_label = ImageLabel()
        self.central_layout.addWidget(self.image_label)
        right_line = QFrame()
        right_line.setFrameShape(QFrame.VLine)
        right_line.setSizePolicy(QSizePolicy.Expanding , QSizePolicy.Minimum)
        right_line.setLineWidth(2)
        right_line.setMinimumSize(3,0)
        self.central_layout.addWidget(right_line)
        right_pane = QWidget(self)
        self.central_layout.addWidget(right_pane)
        self.right_layout = QVBoxLayout(right_pane)
        # Now add things to the right layout
        self.make_djvu_widget = MakeDjvuWidget()
        self.right_layout.addWidget(self.make_djvu_widget)
        top_line = QFrame()
        top_line.setFrameShape(QFrame.HLine)
        top_line.setSizePolicy(QSizePolicy.Minimum , QSizePolicy.Expanding)
        top_line.setLineWidth(2)
        top_line.setMinimumSize(3,0)
        self.right_layout.addWidget(top_line)
        self.titles_widget = TitlesWidget()
        self.right_layout.addWidget(self.titles_widget)
        line_two = QFrame()
        line_two.setFrameShape(QFrame.HLine)
        line_two.setSizePolicy(QSizePolicy.Minimum , QSizePolicy.Expanding)
        line_two.setLineWidth(2)
        line_two.setMinimumSize(3,0)
        self.right_layout.addWidget(line_two)
        self.text_adder = TextAdder(self.image_list)
        self.right_layout.addWidget(self.text_adder)
        line_three = QFrame()
        line_three.setFrameShape(QFrame.HLine)
        line_three.setSizePolicy(QSizePolicy.Minimum , QSizePolicy.Expanding)
        line_three.setLineWidth(2)
        line_three.setMinimumSize(3,0)
        self.right_layout.addWidget(line_three)
        self.right_layout.addItem(
            QSpacerItem(0, 10, QSizePolicy.Minimum, QSizePolicy.Expanding))
        self.summary = SummaryWidget()
        self.right_layout.addWidget(self.summary)

    def setupConnections(self):
        self.image_list_view.activated.connect(self.showImage)
        self.titles_widget.seriesTitleChange.connect(
            self.image_list.setTitlesSeries)
        self.make_djvu_widget.maybeMoreDjvus.connect(self.checkAllDjvus)
        self.summary.updateAllDjvu(self.image_list.all_djvus)
        self.summary.updateAllTitle(self.image_list.all_title)
        self.titles_widget.maybeMoreTitles.connect(self.checkAllTitle)
        self.summary.make_djvu.clicked.connect(self.image_list.make_book)
        self.image_list.txt_source = self.text_adder
        self.image_list.djvuSaved.connect(self.summary.showOk)

    def minimumSizeHint(self):
        return QSize(400, 400)

    def minimumSize(self):
        return QSize(100, 100)

    @pyqtSlot(QModelIndex)
    def showImage(self, index):
        image_item = self.image_list.getItem(index)
        self.image_label.showImage(str(image_item))
        self.make_djvu_widget.setImage(image_item)
        self.titles_widget.setImage(image_item)

    @pyqtSlot()
    def checkAllDjvus(self):
        self.summary.updateAllDjvu(self.image_list.all_djvus)

    @pyqtSlot()
    def checkAllTitle(self):
        self.summary.updateAllTitle(self.image_list.all_title)

    def sizeHint(self):
        return QSize(400, 500)
